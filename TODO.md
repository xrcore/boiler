TODO
====

Classes:
--------

* Keyboard/Joystick controlled Menu
* Possible Tilemap Class? (Only when I make a game that needs it)
* ~2DO~ Boiler should generally delete objects where it is appropriate, instead of letting this be the game's job.

DONE
====

* Reuse old Sprite Code from Entity class into a Sprite Class
* NO MORE WORLD CLASS! (This otherwise forces the games made with 2DO into a specific direction)
* Filesystem Should handle File Loading and Keeping Them in memory. Possibly allowing you access Textures, Sounds, etc. using the FileSystem Class with a String identifier. E.g. "filesystem->loadTexture("data/player.png", "player")" and "filesystem->getTexture("player")". This would also fix some file handling and garbage collection headaches, for example multiple sprites using the same texture, while still being able to be deleted and stuff, because textures can only by unloaded by the filesystem.
* Possibly place sprite animation values inside Entity Class, so that multiple entities can easily share the same texture
