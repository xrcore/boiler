#ifndef TILEMAP_H
#define TILEMAP_H

#include <stdint.h>

namespace Boiler {

#define TILEMAP_COLLIDED_NONE 0
#define TILEMAP_COLLIDED_LEFT 1
#define TILEMAP_COLLIDED_RIGHT 2
#define TILEMAP_COLLIDED_UP 4
#define TILEMAP_COLLIDED_DOWN 8

#define TILEMAP_COLLISION_NONE 0
#define TILEMAP_COLLISION_SOLID 1
#define TILEMAP_COLLISION_JUMPTHROUGH 2

class Tilemap {
	byte * tiledata;
	byte * collisiondata;
	uint32_t width;
	uint32_t height;
	double tilesize;
	byte outsideTile;
	bool outsideCollision;
public:
	Tilemap(uint32_t width, uint32_t height, double tilesize, byte defaultValue);
	~Tilemap();
	void SetTile(uint32_t x, uint32_t y, byte value);
	void SetTileAtWorld(double x, double y, byte value);
	byte GetTile(uint32_t x, uint32_t y);
	byte GetTileAtWorld(double x, double y);
	void SetTileCollision(uint32_t x, uint32_t y, byte collision);
	void SetTileCollisionAtWorld(double x, double y, byte collision);
	byte GetTileCollision(uint32_t x, uint32_t y);
	byte GetTileCollisionAtWorld(double x, double y);
	byte * GetTiledata() { return tiledata; }
	uint32_t GetWidth() { return width; }
	uint32_t GetHeight() { return height; }
	double GetTilesize() { return tilesize; }
	void SetOutsideTile(byte tileId) { outsideTile = tileId; }
	void SetOutsideCollision(bool collision) { outsideCollision = collision; }
	uint8_t MoveAABBAgainstTilemap(double x, double y, double width, double height, double deltaX, double deltaY, double * outX, double * outY);
};

}

#endif
