#ifndef MENU_H
#define MENU_H

typedef unsigned char byte;
#include <vector>
#include <string>

namespace Boiler {

class Menu;
class Engine;
class Font;

class Menu {
private:
	bool isActivationKeyPressed;
	unsigned int selectedButton;
	unsigned int currentButton;
	int buttonHeight;
	int x;
	int y;
	Engine * engine;
	Font * font;
	bool freeFont;
	byte selectedR, selectedG, selectedB;
	byte r, g, b;
	std::string buttonMoveUp;
	std::string buttonMoveDown;
	std::string buttonActivate;
public:
	Menu(Engine * engine);
	~Menu();
	void Start();
	void End();

	bool DoButton(std::string label);

	void SetFont(Font * font, bool free = false);
	void SetButtonHeight(int buttonHeight);
	void SetX(int x);
	void SetY(int y);
	void SetColor(byte r, byte g, byte b);
	void SetSelectedColor(byte r, byte g, byte b);
	void SetButtonUp(std::string button);
	void SetButtonDown(std::string button);
	void SetButtonActivate(std::string button);

	Font * GetFont() { return font; }
	Engine * GetEngine() { return engine; }
};

}

#endif
