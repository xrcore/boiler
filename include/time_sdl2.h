#ifndef TIME_SDL2_H
#define TIME_SDL2_H

#include "boiler.h"

namespace Boiler {

class Time_SDL2 : public Time {
private:
	uint32_t ticks;
	uint32_t lastFrameTicks;
public:
	Time_SDL2(Engine * engine);
	~Time_SDL2();
	void Update();
	void WaitForEndOfFrame();
};

}

#endif
