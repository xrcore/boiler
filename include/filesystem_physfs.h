#ifndef FILESYSTEM_PHYSFS_H
#define FILESYSTEM_PHYSFS_H

#include "filesystem.h"

namespace Boiler {

class Filesystem_PhysFS : public Filesystem {
public:
	Filesystem_PhysFS(Engine * engine, char * argv0);
	virtual ~Filesystem_PhysFS();
	void AddPackage(std::string filename);
	File * GetFile(std::string filename);
};

}

#endif
