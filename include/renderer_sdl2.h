#ifndef RENDERER_SDL2_H
#define RENDERER_SDL2_H

#include "boiler.h"

struct SDL_Window;
struct SDL_Renderer;
struct SDL_Texture;
typedef struct _TTF_Font TTF_Font;

namespace Boiler {

class File;

class Renderer_SDL2 : public Renderer {
private:
	std::vector<SDL_Texture *> textures;
	std::string title;
	SDL_Window * window;
	SDL_Renderer * renderer;
	SDL_Texture * canvas;
	void DestroyAllTextures();
	unsigned int PutTextureInVector(SDL_Texture * texture);
	void ApplyColorModToTexture(SDL_Texture * texture);
	byte colorModR;
	byte colorModG;
	byte colorModB;
public:
	Renderer_SDL2(Engine * engine, unsigned int width, unsigned int height, uint32_t flags);
	virtual ~Renderer_SDL2();
	void ToggleFullscreen();
	void BeginFrame();
	void EndFrame();
	void SetDrawColor(byte r, byte g, byte b);
	void DrawPoint(int x, int y);
	void DrawTexture(Texture * texture, int x, int y);
	void DrawTexture(Texture * texture, int dstX, int dstY, int dstW, int dstH);
	void DrawTexture(Texture * texture, int dstX, int dstY, int dstW, int dstH, int srcX, int srcY, int srcW, int srcH);
	void DrawSprite(Sprite * sprite, int x, int y, double angle = 0.0, int cellX = 0, int cellY = 0, bool flipH = false, bool flipV = false);
	// void DrawTile(Tileset * tileset, uint32_t x, uint32_t y, int tileSize, byte tileId);
	// void DrawTilemap(Tileset * tileset, uint32_t sizeX, uint32_t sizeY, int tileSize, byte * tileData);
	void DrawTile(Tileset * tileset, uint32_t x, uint32_t y, byte tileId);
	void DrawTilemap(Tilemap * tilemap, Tileset * tileset);
	void DrawChar(char character, Font * font, int x, int y);
	void DrawText(std::string text, Font * font, int x, int y);
	Texture * LoadTexture(std::string filename);
	void DestroyTexture(Texture * texture);
	void SetSize(unsigned int width, unsigned int height);
	void SetTitle(std::string title);
	unsigned int GetWindowWidth();
	unsigned int GetWindowHeight();
};

}

#endif
