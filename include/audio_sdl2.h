#ifndef AUDIO_SDL2_H
#define AUDIO_SDL2_H

#include "audio.h"
#include <vector>

struct Mix_Chunk;
typedef struct _Mix_Music Mix_Music;

namespace Boiler {

class File;

class Audio_SDL2 : public Audio {
private:
	std::vector<Mix_Chunk *> clips;
	Mix_Music * music;
	File * musicFile;
	uint32_t PutClipIntoVector(Mix_Chunk * chunk);
public:
	Audio_SDL2(Engine * engine);
	virtual ~Audio_SDL2();
	AudioClip * LoadClip(std::string filename);
	void UnloadClip(AudioClip * clip);
	void PlayClip(AudioClip * clip);
	void PlayMusic(std::string filename, bool loop = true);
	void StopMusic();
};

}

#endif
