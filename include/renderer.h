#ifndef RENDERER_H
#define RENDERER_H

#define RENDERER_VSYNC 1

#include "boiler.h"

namespace Boiler {

class Renderer;
class Tilemap;
class Tileset;

class Texture {
private:
	Renderer * renderer;
	uint32_t id;
	int width;
	int height;
public:
	Texture(Renderer * renderer, uint32_t id, unsigned int width, unsigned int height);
	~Texture();
	uint32_t GetId() { return id; }
	int GetWidth() { return width; }
	int GetHeight() { return height; }
};

class Sprite {
private:
	Texture * texture;
public:
	Sprite(Texture * newTexture) {
		texture = newTexture;
		cellWidth = texture->GetWidth();
		cellHeight = texture->GetHeight();
		cellCenterX = cellWidth / 2;
		cellCenterY = cellHeight / 2;
		cellCountX = 1;
		cellCountY = 1;
	}
	Texture * GetTexture() {
		return texture;
	}

	int cellCenterX;
	int cellCenterY;
	int cellWidth;
	int cellHeight;
	int cellCountX;
	int cellCountY;
};

class Tileset {
private:
	Texture * texture;
	int countX;
	int countY;
	int tileSize;
public:
	Tileset(Texture * texture, int countX, int countY, int tileSize);
	~Tileset();
	Texture * GetTexture() { return texture; }
	int GetCountX() { return countX; }
	int GetCountY() { return countY; }
	int GetTileSize() { return tileSize; }
};

class Font {
private:
	Texture * texture;
	int charCountX;
	int charCountY;
	int charWidth;
	int charHeight;
public:
	Font(Texture * texture, int charCountX, int charCountY, int charWidth, int charHeight);
	Texture * GetTexture() { return texture; }
	int GetCharCountX() { return charCountX; }
	int GetCharCountY() { return charCountY; }
	int GetCharWidth() { return charWidth; }
	int GetCharHeight() { return charHeight; }
};

class Renderer {
protected:
	unsigned int width;
	unsigned int height;
	bool fullscreen;
	Engine * engine;
public:
	Renderer(Engine * engine, unsigned int width, unsigned int height, uint32_t flags);
	virtual ~Renderer() {}
	virtual void ToggleFullscreen() { fullscreen = !fullscreen; }
	virtual void BeginFrame() {}
	virtual void EndFrame() {}
	virtual void SetDrawColor(byte r, byte g, byte b) {}
	virtual void DrawPoint(int x, int y) {}
	virtual void DrawTexture(Texture * texture, int x, int y) {}
	virtual void DrawTexture(Texture * texture, int dstX, int dstY, int dstW, int dstH) {}
	virtual void DrawTexture(Texture * texture, int dstX, int dstY, int dstW, int dstH, int srcX, int srcY, int srcW, int srcH) {}
	virtual void DrawSprite(Sprite * sprite, int x, int y, double angle = 0.0, int cellX = 0, int cellY = 0, bool flipH = false, bool flipV = false) {}
	virtual void DrawTile(Tileset * tileset, uint32_t x, uint32_t y, byte tileId) {}
	virtual void DrawTilemap(Tilemap * tilemap, Tileset * tileset) {}
	virtual void DrawChar(char character, Font * font, int x, int y) {}
	virtual void DrawText(std::string text, Font * font, int x, int y) {}
	virtual Texture * LoadTexture(std::string filename);
	virtual void DestroyTexture(Texture * texture) {}
	virtual void SetSize(unsigned int width, unsigned int height);
	virtual void SetTitle(std::string title) {}
	unsigned int GetWidth() { return width; }
	unsigned int GetHeight() { return height; }
	virtual unsigned int GetWindowWidth() { return 1; }
	virtual unsigned int GetWindowHeight() { return 1; }

	double cameraX;
	double cameraY;
};

}

#endif
