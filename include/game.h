#ifndef GAME_H
#define GAME_H

namespace Boiler {

class Engine;
class Renderer;
class Time;
class Input;
class Audio;
class Filesystem;
class Gui;

class Game {
protected:
	Engine * engine;

public:
	Game() {}
	virtual ~Game() {}
	virtual void OnStart() {}
	virtual void OnQuit() {}
	virtual void OnUpdate() {}
	virtual void OnFixedUpdate() {}
	virtual void OnDraw() {}
	void SetEngine(Engine * newEngine) { engine = newEngine; }
	Engine * GetEngine() { return engine; }

	//These are simply shorthand for writing GetEngine()->GetBlabla()
	Renderer * GetRenderer() ;
	Time * GetTime();
	Input * GetInput();
	Audio * GetAudio();
	Filesystem * GetFilesystem();
	Gui * GetGui();
	Menu * GetMenu();
};

}

#endif
