#ifndef MATH_H
#define MATH_H

namespace Boiler {

void Vec2_Rotate(double * x, double * y, double angle);
double Vec2_Angle(double x, double y);
double Vec2_SqrMagnitude(double x, double y);
double Vec2_Magnitude(double x, double y);
double Vec2_SqrDistance(double x1, double y1, double x2, double y2);
double Vec2_Distance(double x1, double y1, double x2, double y2);
void Vec2_Normalize(double * x, double * y);
double Lerp(double a, double b, double t);
double Clamp(double v, double min, double max);
int Clamp(int v, int min, int max);

}

#endif
