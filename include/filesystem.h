#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <string>
#include <map>
#include <list>

namespace Boiler {

class Texture;
class AudioClip;

class File {
private:
	char * data;
	size_t filesize;
public:
	File(char * newData, size_t newFilesize) {
		data = newData;
		filesize = newFilesize;
	}
	~File() {
		delete[] data;
	}
	char * GetData() { return data; }
	size_t GetSize() { return filesize; }
};

class Filesystem {
protected:
	std::map<std::string, Texture *> textures;
	std::map<std::string, AudioClip *> audioClips;
	std::list<std::string> packages;
	Engine * engine;
public:
	Filesystem(Engine * engine);
	virtual ~Filesystem();
	virtual void AddPackage(std::string filename);
	virtual File * GetFile(std::string filename);
	void LoadTexture(std::string filename, std::string identifier);
	void LoadAudioClip(std::string filename, std::string identifier);
	Texture * GetTexture(std::string identifier);
	AudioClip * GetAudioClip(std::string identifier);
	void UnloadAllTextures();
	void UnloadAllAudioClips();
};

}

#endif
