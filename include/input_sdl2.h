#ifndef INPUT_SDL2_H
#define INPUT_SDL2_H

#include "boiler.h"
#include <map>
#include <vector>

typedef struct _SDL_Joystick SDL_Joystick;
typedef int SDL_Keycode;
union SDL_Event;

namespace Boiler {

class Input_SDL2 : public Input {
private:
    std::map<SDL_Keycode, std::string> keycodeNameLookup;
public:
	Input_SDL2(Engine * engine);
	void Update();

    bool SetKey(std::string key, std::string name);
};

}

#endif
