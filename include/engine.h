#ifndef ENGINE_H
#define ENGINE_H

#include <string>

namespace Boiler {

class Renderer;
class Time;
class Input;
class Game;
class Filesystem;
class Audio;
class Menu;

class Engine {
private:
	int argc;
	char ** argv;
	Renderer * renderer;
	Time * time;
	Input * input;
	Audio * audio;
	Filesystem * filesystem;
	Gui * gui;
	Menu * menu;
	Game * game;
	bool initialized;
	bool running;
	std::string rendererName;
	std::string timerName;
	std::string inputName;
	std::string audioName;
	std::string filesystemName;
public:
	Engine(int argc, char* argv[]);
	~Engine();
	void Init(int width, int height, uint32_t rendererFlags);
	void InitGame(Game * game);
	void Run();
	void Log(std::string text, int level = 2, bool error = false);
	Renderer * GetRenderer() { return renderer; }
	Time * GetTime() { return time; }
	Input * GetInput() { return input; }
	Audio * GetAudio() { return audio; }
	Filesystem * GetFilesystem() { return filesystem; }
	Gui * GetGui() { return gui; }
	Menu * GetMenu() { return menu; }
	Game * GetGame() { return game; }

	void SetRendererName(std::string name);
	void SetTimerName(std::string name);
	void SetInputName(std::string name);
	void SetAudioName(std::string name);
	void SetFilesystemName(std::string name);
	/*
	LogLevel 1: Regular
	LogLevel 2: Verbose
	*/
	int loglevel;
};

}

#endif
