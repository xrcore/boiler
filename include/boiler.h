#ifndef BOILER_H
#define BOILER_H

#include <iostream>
#include <cstdint>
#define _USE_MATH_DEFINES
#include <cmath>
#include <map>
#include <vector>
#include <string>

typedef unsigned char byte;

#define BOILER_VERSION_MAJOR 0
#define BOILER_VERSION_MINOR 1
#define BOILER_VERSION_PATCH 0

#define BOILER_DEFAULT_RENDERER "sdl2"
#define BOILER_DEFAULT_TIMER "sdl2"
#define BOILER_DEFAULT_INPUT "sdl2"
#define BOILER_DEFAULT_AUDIO "sdl2"
#define BOILER_DEFAULT_FILESYSTEM "physfs"

#include "boiler_math.h"
#include "time.h"
#include "input.h"
#include "renderer.h"
#include "filesystem.h"
#include "gui.h"
#include "menu.h"
#include "engine.h"
#include "audio.h"
#include "tilemap.h"

#include "time_sdl2.h"
#include "input_sdl2.h"
#include "renderer_sdl2.h"
#include "audio_sdl2.h"

#include "filesystem_physfs.h"

#include "game.h"

#endif
