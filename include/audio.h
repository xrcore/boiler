#ifndef AUDIO_H
#define AUDIO_H

#include <string>

namespace Boiler {

class Audio;

class AudioClip {
private:
	Audio * audio;
	uint32_t id;
public:
	AudioClip(Audio * audio, uint32_t id);
	~AudioClip();
	uint32_t GetId() { return id; }
};

class Audio {
protected:
	Engine * engine;
public:
	Audio(Engine * newEngine) { engine = newEngine; }
	virtual ~Audio() {}
	virtual AudioClip * LoadClip(std::string filename) { return new AudioClip(this, 0); }
	virtual void UnloadClip(AudioClip * clip) {}
	virtual void PlayClip(AudioClip * clip) {}
	virtual void PlayMusic(std::string filename, bool loop = true) {}
	virtual void StopMusic() {}
};

}

#endif
