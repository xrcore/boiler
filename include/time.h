#ifndef TIME_H
#define TIME_H

namespace Boiler {

class Engine;

class Time {
protected:
	double time;
	double deltaTime;
	int framerate;
	double lastFixedUpdate;
	double fixedDeltaTime;
	Engine * engine;
public:
	Time(Engine * engine);
	virtual ~Time() {}
	virtual void Update() {}
	int GetFixedUpdateCount();
	double GetTime() { return time; }
	double GetDeltaTime() { return deltaTime; }
	double GetFPS() { return 1.0 / deltaTime; }
	virtual void WaitForEndOfFrame() {}
	void SetFramerate(int newFramerate) { framerate = newFramerate; }
	void SetFixedDeltaTime(double newFixedDeltaTime) { fixedDeltaTime = newFixedDeltaTime; }
	double GetFixedDeltaTime() { return fixedDeltaTime; }
};

}

#endif
