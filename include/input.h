#ifndef INPUT_H
#define INPUT_H

#include "boiler.h"
#include <string>
#include <map>

namespace Boiler {

class Engine;

class Input {
protected:
	Engine * engine;
	std::map<std::string, bool> keys;
	std::map<std::string, bool> keysDown;
	bool requestedQuit;
	bool requestedFullscreen;
	int mouseX;
	int mouseY;
	bool mouseLeft;
	bool mouseRight;
public:
	Input(Engine * newEngine) {
		engine = newEngine;
		requestedQuit = false;
		requestedFullscreen = false;
		mouseX = 0;
		mouseY = 0;
		mouseLeft = false;
		mouseRight = false;
	}
	virtual ~Input() {}
	virtual void Update() {}
	bool GetRequestedQuit() { return requestedQuit; }
	bool GetRequestedFullscreen() { return requestedFullscreen; }
	int GetMouseX() { return mouseX; }
	int GetMouseY() { return mouseY; }
	bool GetMouseLeft() { return mouseLeft; }
	bool GetMouseRight() { return mouseRight; }
	virtual bool SetKey(std::string key, std::string name) { return false; }
	bool GetKeyPressed(std::string name) { return keys[name]; }
	bool GetKeyDown(std::string name) { return keysDown[name]; }
};

}

#endif
