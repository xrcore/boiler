#ifndef GUI_H
#define GUI_H

#include <list>
#include <string>

namespace Boiler {

class Texture;
class Renderer;
class Engine;

class GuiElement {
private:
	Renderer * renderer;
	Texture * texture;
	std::string text;
	std::string font;
	void Update();
public:
	GuiElement(Renderer * renderer, std::string font);
	~GuiElement();
	void SetColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255);
	void SetContent(std::string text);
	Texture * GetTexture() { return texture; }

	bool visible;
	int x;
	int y;
	unsigned char r, g, b, a;
};

class Gui {
private:
	Engine * engine;
	std::list<GuiElement *> elements;
public:
	Gui(Engine * engine);
	~Gui();
	void AddElement(GuiElement * element);
	void RemoveElement(GuiElement * element);
	void Draw();
};

}

#endif
