INCLUDE = -Iinclude/
OBJ_ENGINE = source/*.o

prog:
	cd source; $(MAKE) $(MAKEFLAGS); cd ..;
	ar rcs libboiler.a $(OBJ_ENGINE)
	
.PHONY: clean
clean:
	rm source/*.o
	rm libboiler.a
