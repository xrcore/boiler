#include "boiler.h"
#include "SDL2/SDL_mixer.h"

using namespace Boiler;

Audio_SDL2::Audio_SDL2(Engine * engine) : Audio(engine) {
	music = NULL;
	musicFile = NULL;
	Mix_Init(MIX_INIT_MOD);
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 4, 1024);
}

Audio_SDL2::~Audio_SDL2() {
	Mix_CloseAudio();
	Mix_Quit();
}

uint32_t Audio_SDL2::PutClipIntoVector(Mix_Chunk * chunk) {
	for(uint32_t i = 0; i < clips.size(); i++) {
		if(clips[i] == NULL) {
			clips[i] = chunk;
			return i;
		}
	}
	clips.push_back(chunk);
	return clips.size() - 1;
}

AudioClip * Audio_SDL2::LoadClip(std::string filename) {
	engine->Log("Loading Audio Clip from file: " + filename);
	File * soundFile = engine->GetFilesystem()->GetFile(filename);
	SDL_RWops * soundRW = SDL_RWFromMem(soundFile->GetData(), soundFile->GetSize());
	Mix_Chunk * chunk = Mix_LoadWAV_RW(soundRW, 1);
	delete soundFile;
	uint32_t id = PutClipIntoVector(chunk);
	return new AudioClip(this, id);
}

void Audio_SDL2::UnloadClip(AudioClip * clip) {
	uint32_t id = clip->GetId();
	Mix_Chunk * chunk = clips[id];
	Mix_FreeChunk(chunk);
	clips[id] = NULL;
}

void Audio_SDL2::PlayClip(AudioClip * clip) {
	uint32_t id = clip->GetId();
	Mix_Chunk * chunk = clips[id];
	Mix_PlayChannel(-1, chunk, 0);
}

void Audio_SDL2::PlayMusic(std::string filename, bool loop) {
	engine->Log("Playing Music from file: " + filename);
	StopMusic();
	musicFile = engine->GetFilesystem()->GetFile(filename);
	SDL_RWops * musicRW = SDL_RWFromMem(musicFile->GetData(), musicFile->GetSize());
	music = Mix_LoadMUS_RW(musicRW, 1);
	int loops = loop ? -1 : 1;
	Mix_PlayMusic(music, loops);
}

void Audio_SDL2::StopMusic() {
	if(music != NULL) {
		Mix_HaltMusic();
		Mix_FreeMusic(music);
		delete musicFile;
	}
}
