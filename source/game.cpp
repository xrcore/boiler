#include "boiler.h"

using namespace Boiler;

Renderer * Game::GetRenderer() {
	return engine->GetRenderer();
}

Time * Game::GetTime() {
	return engine->GetTime();
}

Input * Game::GetInput() {
	return engine->GetInput();
}

Audio * Game::GetAudio() {
	return engine->GetAudio();
}

Filesystem * Game::GetFilesystem() {
	return engine->GetFilesystem();
}

Gui * Game::GetGui() {
	return engine->GetGui();
}

Menu * Game::GetMenu() {
	return engine->GetMenu();
}
