#include "boiler.h"
#include "physfs.h"

using namespace Boiler;

Filesystem_PhysFS::Filesystem_PhysFS(Engine * engine, char * argv0) : Filesystem(engine) {
	PHYSFS_init(argv0);
}

Filesystem_PhysFS::~Filesystem_PhysFS() {
	PHYSFS_deinit();
}

void Filesystem_PhysFS::AddPackage(std::string filename) {
	engine->Log("Adding package: " + filename);
	PHYSFS_addToSearchPath(filename.c_str(), 1);
}

File * Filesystem_PhysFS::GetFile(std::string filename) {
	engine->Log("Accessing file: " + filename);
	if(!PHYSFS_exists(filename.c_str())) {
		return NULL;
	}
	PHYSFS_file * physfile = PHYSFS_openRead(filename.c_str());
	PHYSFS_sint64 file_size = PHYSFS_fileLength(physfile);
	char * filebuf = new char[file_size];
	PHYSFS_read(physfile, filebuf, 1, file_size);
	PHYSFS_close(physfile);
	return new File(filebuf, file_size);
}
