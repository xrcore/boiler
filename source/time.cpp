#include "boiler.h"

using namespace Boiler;

Time::Time(Engine * newEngine) {
	engine = newEngine;
	framerate = 60;
	fixedDeltaTime = 0.05;
	deltaTime = 0.0;
	lastFixedUpdate = 0.0;
}

int Time::GetFixedUpdateCount() {
	int fixedUpdateCount = floor((time - lastFixedUpdate) / fixedDeltaTime);
	lastFixedUpdate += fixedUpdateCount * fixedDeltaTime;
	return fixedUpdateCount;
}
