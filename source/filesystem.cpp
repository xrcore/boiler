#include "boiler.h"
#include <fstream>

using namespace Boiler;

Filesystem::Filesystem(Engine * newEngine) {
	engine = newEngine;
}

Filesystem::~Filesystem() {

}

void Filesystem::AddPackage(std::string filename) {
	packages.push_back(filename);
}

bool FileExists (std::string filename) {
	std::ifstream file(filename);
	return file.good();
}

File * Filesystem::GetFile(std::string filename) {
	// Iterate through packages, prepend path of package to filename
	// Use first resulting filename that exists
	for(auto it = packages.begin(); it != packages.end(); ++it) {
		std::string fullFilename = *it + "/" + filename;
		if(FileExists(fullFilename)) {
			std::ifstream file;		
			file.open(fullFilename, std::ifstream::in | std::ifstream::binary);
			file.seekg(0, file.end);
			int size = file.tellg();
			file.seekg(0, file.beg);
			char * data = new char[size];
			file.read(data, size);
			file.close();
			return new File(data, size);
		}
	}
	return NULL;
}

void Filesystem::LoadTexture(std::string filename, std::string identifier) {
	if(textures.count(identifier) > 0) {
		return;
	}
	textures[identifier] = engine->GetRenderer()->LoadTexture(filename);
}

void Filesystem::LoadAudioClip(std::string filename, std::string identifier) {
	if(audioClips.count(identifier) > 0) {
		return;
	}
	audioClips[identifier] = engine->GetAudio()->LoadClip(filename);
}

Texture * Filesystem::GetTexture(std::string identifier) {
	if(textures.count(identifier) == 0) {
		return NULL;
	}
	return textures[identifier];
}

AudioClip * Filesystem::GetAudioClip(std::string identifier) {
	if(audioClips.count(identifier) == 0) {
		return NULL;
	}
	return audioClips[identifier];
}

void Filesystem::UnloadAllTextures() {
	for(auto it = textures.begin(); it != textures.end(); ++it) {
		delete it->second;
	}
	textures.clear();
}

void Filesystem::UnloadAllAudioClips() {
	for(auto it = audioClips.begin(); it != audioClips.end(); ++it) {
		delete it->second;
	}
	audioClips.clear();
}
