#include "boiler.h"
#include <algorithm>

using namespace Boiler;

void Boiler::Vec2_Rotate(double * x, double * y, double angle) {
	double cs = cos(angle);
	double sn = sin(angle);

	double nx = *x * cs - *y * sn;
	double ny = *x * sn + *y * cs;

	*x = nx;
	*y = ny;
}

double Boiler::Vec2_Angle(double x, double y) {
	return atan2(y, x);
}

double Boiler::Vec2_SqrMagnitude(double x, double y) {
	return x*x + y*y;
}

double Boiler::Vec2_Magnitude(double x, double y) {
	return sqrt(Vec2_SqrMagnitude(x, y));
}

double Boiler::Vec2_SqrDistance(double x1, double y1, double x2, double y2) {
	return Vec2_SqrMagnitude(x2 - x1, y2 - y1);
}

double Boiler::Vec2_Distance(double x1, double y1, double x2, double y2) {
	return Vec2_Magnitude(x2 - x1, y2 - y1);
}

void Boiler::Vec2_Normalize(double * x, double * y) {
	double mag = Vec2_Magnitude(*x, *y);
	*x /= mag;
	*y /= mag;
}

double Boiler::Lerp(double a, double b, double t) {
	return a + (b-a)*t;
}

double Boiler::Clamp(double v, double min, double max) {
	return std::max(min, std::min(max, v));
}

int Boiler::Clamp(int v, int min, int max) {
	return std::max(min, std::min(max, v));
}

