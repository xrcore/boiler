#include "boiler.h"
#include <cstring>

using namespace Boiler;

Engine::Engine(int newArgc, char* newArgv[]) {
	argc = newArgc;
	argv = newArgv;
	loglevel = 1;
	for(int i = 0; i < argc; i++) {
		if(strcmp(argv[i], "-verbose") == 0) {
			loglevel = 2;
		}
	}
	initialized = false;
	running = false;
	rendererName = BOILER_DEFAULT_RENDERER;
	timerName = BOILER_DEFAULT_TIMER;
	inputName = BOILER_DEFAULT_INPUT;
	audioName = BOILER_DEFAULT_AUDIO;
	filesystemName = BOILER_DEFAULT_FILESYSTEM;
}

Engine::~Engine() {
	Log("Unloading Engine", 1);
	if(initialized) {
		Log("Unloading Game");
		delete game;
		Log("Unloading Timer");
		delete time;
		Log("Unloading Input");
		delete input;
		Log("Unloading Audio");
		delete audio;
		Log("Unloading Menu");
		delete menu;
		Log("Unloading GUI");
		delete gui;
		Log("Unloading Renderer");
		delete renderer;
		Log("Unloading Filesystem");
		delete filesystem;
	}
}

void Engine::Init(int width, int height, uint32_t rendererFlags) {
	#ifdef __MINGW32__
		Log("Initializing Boiler", 1);
	#else
		Log("Initializing Boiler " + std::to_string(BOILER_VERSION_MAJOR) + "." + std::to_string(BOILER_VERSION_MINOR) + "." + std::to_string(BOILER_VERSION_PATCH), 1);
	#endif
	Log("Initializing Renderer");
	if(rendererName.compare("sdl2") == 0) {
		renderer = new Renderer_SDL2(this, width, height, rendererFlags);
	} else {
		renderer = new Renderer(this, width, height, rendererFlags);
	}
	Log("Initializing Timer");
	if(timerName.compare("sdl2") == 0) {
		time = new Time_SDL2(this);
	} else {
		time = new Time(this);
	}
	Log("Initializing Input");
	if(inputName.compare("sdl2") == 0) {
		input = new Input_SDL2(this);
	} else {
		input = new Input(this);
	}
	Log("Initializing Audio");
	if(audioName.compare("sdl2") == 0) {
		audio = new Audio_SDL2(this);
	} else {
		audio = new Audio(this);
	}
	Log("Initializing Filesystem");
	if(filesystemName.compare("physfs") == 0) {
		filesystem = new Filesystem_PhysFS(this, argv[0]);
	} else {
		filesystem = new Filesystem(this);
	}
	Log("Creating empty GUI");
	gui = new Gui(this);
	Log("Creating empty Menu");
	menu = new Menu(this);

	initialized = true;
}

void Engine::InitGame(Game * newGame) {
	Log("Setting Game instance");
	game = newGame;
	game->SetEngine(this);
}

void Engine::Run() {
	Log("Starting Game");
	if(running) {
		return;
	}
	bool running = true;
	Log("Initializing Game");
	game->OnStart();
	Log("Beginning main loop");
	while(running) {
		time->Update();
		input->Update();
		game->OnUpdate();
		int fixedUpdateCount = time->GetFixedUpdateCount();
		for(int i = 0; i < fixedUpdateCount; i++) {
			game->OnFixedUpdate();
		}
		renderer->BeginFrame();
		game->OnDraw();
		gui->Draw();
		renderer->EndFrame();
		if(input->GetRequestedQuit()) {
			running = false;
		}
		if(input->GetRequestedFullscreen()) {
			renderer->ToggleFullscreen();
		}
		time->WaitForEndOfFrame();
	}
	Log("Quitting Game");
	game->OnQuit();
}

void Engine::Log(std::string text, int level, bool error) {
	std::string prefix = "[LOG]";
	if(error) {
		prefix = "[ERR]";
	}
	if(level <= loglevel) {
		std::cout << prefix << " " << text << std::endl;
	}
}

void Engine::SetRendererName(std::string name) {
	if(initialized) {
		Log("Warning: Trying to change renderer name, but the engine is already initialized!", 1, true);
		return;
	}
	rendererName = name;
}

void Engine::SetTimerName(std::string name) {
	if(initialized) {
		Log("Warning: Trying to change timer name, but the engine is already initialized!", 1, true);
		return;
	}
	timerName = name;
}

void Engine::SetInputName(std::string name) {
	if(initialized) {
		Log("Warning: Trying to change input name, but the engine is already initialized!", 1, true);
		return;
	}
	inputName = name;
}

void Engine::SetAudioName(std::string name) {
	if(initialized) {
		Log("Warning: Trying to change audio name, but the engine is already initialized!", 1, true);
		return;
	}
	audioName = name;
}

void Engine::SetFilesystemName(std::string name) {
	if(initialized) {
		Log("Warning: Trying to change filesystem name, but the engine is already initialized!", 1, true);
		return;
	}
	filesystemName = name;
}
