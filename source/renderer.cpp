#include "boiler.h"

using namespace Boiler;

Texture::Texture(Renderer * newRenderer, uint32_t newId, unsigned int newWidth, unsigned int newHeight) {
	renderer = newRenderer;
	id = newId;
	width = newWidth;
	height = newHeight;
}

Texture::~Texture() {
	renderer->DestroyTexture(this);
}

Tileset::Tileset(Texture * newTexture, int newCountX, int newCountY, int newTileSize) {
	texture = newTexture;
	countX = newCountX;
	countY = newCountY;
	tileSize = newTileSize;
}

Tileset::~Tileset() {
	delete texture;
}

Renderer::Renderer(Engine * newEngine, unsigned int newWidth, unsigned int newHeight, uint32_t flags) {
	engine = newEngine;
	width = newWidth;
	height = newHeight;
	fullscreen = false;
	cameraX = 0;
	cameraY = 0;
}

Texture * Renderer::LoadTexture(std::string filename) {
	return new Texture(this, 0, 0, 0);
}

void Renderer::SetSize(unsigned int newWidth, unsigned int newHeight) {
	width = newWidth;
	height = newHeight;
}

Font::Font(Texture * newTexture, int newCharCountX, int newCharCountY, int newCharWidth, int newCharHeight) {
	texture = newTexture;
	charCountX = newCharCountX;
	charCountY = newCharCountY;
	charWidth = newCharWidth;
	charHeight = newCharHeight;
}