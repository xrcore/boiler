#include "boiler.h"

using namespace Boiler;

Menu::Menu(Engine * newEngine) {
	engine = newEngine;
	isActivationKeyPressed = false;
	x = 0;
	y = 0;
	r = 100;
	g = 100;
	b = 100;
	selectedButton = 0;
	currentButton = 0;
	selectedR = 255;
	selectedG = 255;
	selectedB = 255;
	font = NULL;
	freeFont = false;
	buttonMoveUp = "";
	buttonMoveDown = "";
	buttonActivate = "";
}

Menu::~Menu() {
	if(font != NULL && freeFont) {
		delete font;
	}
}

void Menu::Start() {
	currentButton = 0;
	isActivationKeyPressed = engine->GetInput()->GetKeyPressed(buttonActivate);
}

void Menu::End() {
	if(currentButton == 0) {
		selectedButton = 0;
		return;
	}
	if(engine->GetInput()->GetKeyPressed(buttonMoveUp)) {
		selectedButton--;
	} else if(engine->GetInput()->GetKeyPressed(buttonMoveDown)) {
		selectedButton++;
	}
	selectedButton = selectedButton % (currentButton);
}

bool Menu::DoButton(std::string label) {
	bool activated = false;
	int buttonX = x;
	int buttonY = y + currentButton * buttonHeight;

	if(selectedButton == currentButton) {
		engine->GetRenderer()->SetDrawColor(selectedR, selectedG, selectedB);
		activated = isActivationKeyPressed;
	} else {
		engine->GetRenderer()->SetDrawColor(r, g, b);
	}
	engine->GetRenderer()->DrawText(label, font, buttonX, buttonY);
	engine->GetRenderer()->SetDrawColor(255, 255, 255);

	currentButton++;
	return activated;
}

void Menu::SetFont(Font * newFont, bool free) {
	font = newFont;
	freeFont = free;
}

void Menu::SetButtonHeight(int newButtonHeight) {
	buttonHeight = newButtonHeight;
}

void Menu::SetX(int newX) {
	x = newX;
}

void Menu::SetY(int newY) {
	y = newY;
}

void Menu::SetColor(byte nr, byte ng, byte nb) {
	r = nr;
	g = ng;
	b = nb;
}

void Menu::SetSelectedColor(byte nr, byte ng, byte nb) {
	selectedR = nr;
	selectedG = ng;
	selectedB = nb;
}

void Menu::SetButtonUp(std::string button) {
	buttonMoveUp = button;
}

void Menu::SetButtonDown(std::string button) {
	buttonMoveDown = button;
}

void Menu::SetButtonActivate(std::string button) {
	buttonActivate = button;
}