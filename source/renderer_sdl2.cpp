#include "boiler.h"
#include "SDL2/SDL.h"
#define STB_IMAGE_IMPLEMENTATION
#define USE_WIERD_BUT_WORKING_PIXEL_FORMAT
#include "stb_image.h"
#include <algorithm>

using namespace Boiler;

Renderer_SDL2::Renderer_SDL2(Engine * engine, unsigned int width, unsigned int height, uint32_t flags) : Renderer(engine, width, height, flags) {
	SDL_Init(SDL_INIT_VIDEO);
	Uint32 windowFlags = SDL_WINDOW_RESIZABLE;
	window = SDL_CreateWindow("Renderer_SDL2", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, windowFlags);
	Uint32 rendererFlags = 0;
	rendererFlags |= SDL_RENDERER_TARGETTEXTURE;
	if((flags & RENDERER_VSYNC) > 0) rendererFlags |= SDL_RENDERER_PRESENTVSYNC;
	renderer = SDL_CreateRenderer(window, -1, rendererFlags);
	canvas = NULL;
	SetSize(width, height);
	SetTitle("Untitled");
	SetDrawColor(255, 255, 255);
}

Renderer_SDL2::~Renderer_SDL2() {
	DestroyAllTextures();
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
}

void Renderer_SDL2::ToggleFullscreen() {
	fullscreen = !fullscreen;
	if(fullscreen) {
		SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
	} else {
		SDL_SetWindowFullscreen(window, 0);
	}
}

void Renderer_SDL2::DestroyAllTextures() {
	for(unsigned int i = 0; i < textures.size(); i++) {
		if(textures[i] != NULL) {
			SDL_DestroyTexture(textures[i]);
		}
	}
}

unsigned int Renderer_SDL2::PutTextureInVector(SDL_Texture * texture) {
	for(unsigned int i = 0; i < textures.size(); i++) {
		if(textures[i] == NULL) {
			textures[i] = texture;
			return i;
		}
	}
	textures.push_back(texture);
	return textures.size() - 1;
}

void Renderer_SDL2::BeginFrame() {
	SDL_SetRenderTarget(renderer, canvas);
	byte tempR, tempG, tempB, tempA;
	SDL_GetRenderDrawColor(renderer, &tempR, &tempG, &tempB, &tempA);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer, tempR, tempG, tempB, tempA);
}

void Renderer_SDL2::EndFrame() {
	SDL_SetRenderTarget(renderer, NULL);
	SDL_RenderCopy(renderer, canvas, NULL, NULL);
	SDL_RenderPresent(renderer);
#ifdef __MINGW32__
	std::string debugTitle = title;
#else
	std::string debugTitle = title + "    FPS: " + std::to_string((int)engine->GetTime()->GetFPS());
#endif
	SDL_SetWindowTitle(window, debugTitle.c_str());
}

void Renderer_SDL2::SetDrawColor(byte r, byte g, byte b) {
	colorModR = r;
	colorModG = g;
	colorModB = b;
	SDL_SetRenderDrawColor(renderer, r, g, b, 255);
}

void Renderer_SDL2::DrawPoint(int x, int y) {
	SDL_RenderDrawPoint(renderer, x, y);
}

void Renderer_SDL2::ApplyColorModToTexture(SDL_Texture * texture) {
	SDL_SetTextureColorMod(texture, colorModR, colorModG, colorModB);
}

void Renderer_SDL2::DrawTexture(Texture * texture, int x, int y) {
	DrawTexture(texture, x, y, texture->GetWidth(), texture->GetHeight(), 0, 0, texture->GetWidth(), texture->GetHeight());
}

void Renderer_SDL2::DrawTexture(Texture * texture, int dstX, int dstY, int dstW, int dstH) {
	DrawTexture(texture, dstX, dstY, dstW, dstH, 0, 0, texture->GetWidth(), texture->GetHeight());
}

void Renderer_SDL2::DrawTexture(Texture * texture, int dstX, int dstY, int dstW, int dstH, int srcX, int srcY, int srcW, int srcH) {
	uint32_t id = texture->GetId();
	SDL_Rect dst = {dstX, dstY, dstW, dstH};
	SDL_Rect src = {srcX, srcY, srcW, srcH};
	ApplyColorModToTexture(textures[id]);
	SDL_RenderCopy(renderer, textures[id], &src, &dst);
}

void Renderer_SDL2::DrawSprite(Sprite * sprite, int x, int y, double angle, int cellX, int cellY, bool flipH, bool flipV) {
	Texture * texture = sprite->GetTexture();
	uint32_t id = texture->GetId();
	SDL_Rect dst = {x - (int)cameraX - sprite->cellCenterX, y - (int)cameraY - sprite->cellCenterY, sprite->cellWidth, sprite->cellHeight};
	int srcX = cellX % sprite->cellCountX * sprite->cellWidth;
	int srcY = cellY % sprite->cellCountY * sprite->cellHeight;
	SDL_Rect src = {srcX, srcY, sprite->cellWidth, sprite->cellHeight};
	ApplyColorModToTexture(textures[id]);
	int flip = SDL_FLIP_NONE;
	if(flipH) flip |= SDL_FLIP_HORIZONTAL;
	if(flipV) flip |= SDL_FLIP_VERTICAL;
	SDL_RenderCopyEx(renderer, textures[id], &src, &dst, angle, NULL, (SDL_RendererFlip)flip);
}

int clamp(int val, int min, int max) {
	return std::max(min, std::min(max, val));
}

void Renderer_SDL2::DrawTile(Tileset * tileset, uint32_t x, uint32_t y, byte tileId) {
	if(tileId == 0) return;
	int tileSize = tileset->GetTileSize();
	int srcX = (tileId - 1) % tileset->GetCountX() * tileSize;
	int srcY = ((tileId - 1) / tileset->GetCountX()) % tileset->GetCountY() * tileSize;
	SDL_Rect dst = {(int)x * tileSize - (int)cameraX, (int)y * tileSize - (int)cameraY, tileSize, tileSize};
	SDL_Rect src = {srcX, srcY, tileSize, tileSize};
	uint32_t texid = tileset->GetTexture()->GetId();
	ApplyColorModToTexture(textures[texid]);
	SDL_RenderCopy(renderer, textures[texid], &src, &dst);
}

void Renderer_SDL2::DrawTilemap(Tilemap * tilemap, Tileset * tileset) {
	uint32_t xStart = clamp((int)cameraX / tileset->GetTileSize(), 0, tilemap->GetWidth());
	uint32_t xEnd = clamp(xStart + width/tileset->GetTileSize() + 1, 0, tilemap->GetWidth());
	uint32_t yStart = clamp((int)cameraY / tileset->GetTileSize(), 0, tilemap->GetHeight());
	uint32_t yEnd = clamp(yStart + height/tileset->GetTileSize() + 2, 0, tilemap->GetHeight());
	byte * tileData = tilemap->GetTiledata();
	for(uint32_t y = yStart; y < yEnd; y++) {
		for(uint32_t x = xStart; x < xEnd; x++) {
			byte tileId = tileData[y * tilemap->GetWidth() + x];
			DrawTile(tileset, x, y, tileId);
		}
	}
}

void Renderer_SDL2::DrawChar(char character, Font * font, int x, int y) {
	SDL_Rect src, dst;
	src.x = (character % font->GetCharCountX()) * font->GetCharWidth();
	src.y = (character / font->GetCharCountX()) * font->GetCharHeight();
	src.w = font->GetCharWidth();
	src.h = font->GetCharHeight();
	dst.x = x;
	dst.y = y;
	dst.w = font->GetCharWidth();
	dst.h = font->GetCharHeight();
	SDL_RenderCopy(renderer, textures[font->GetTexture()->GetId()], &src, &dst);
}

void Renderer_SDL2::DrawText(std::string text, Font * font, int x, int y) {
	unsigned int curX = x;
	unsigned int curY = y;
	ApplyColorModToTexture(textures[font->GetTexture()->GetId()]);
	for(unsigned long i = 0; i < text.size(); i++) {
		if(text[i] == '\n') {
			curY += font->GetCharHeight();
			curX = x;
		} else {
			DrawChar(text[i], font, curX, curY);
			curX += font->GetCharWidth();
		}
	}
}

Texture * Renderer_SDL2::LoadTexture(std::string filename) {
	engine->Log("Loading Texture from file: " + filename);
	int x,y,n;
	File * file = engine->GetFilesystem()->GetFile(filename);
	unsigned char *data = stbi_load_from_memory(reinterpret_cast<unsigned char *>(file->GetData()), file->GetSize(), &x, &y, &n, 4);
	delete file;
	Uint32 format = SDL_PIXELFORMAT_RGBA8888;
	#ifdef USE_WIERD_BUT_WORKING_PIXEL_FORMAT
	format = SDL_PIXELFORMAT_ABGR8888;
	#endif
	SDL_Texture * texture = SDL_CreateTexture(renderer, format, SDL_TEXTUREACCESS_STATIC, x, y);
	SDL_UpdateTexture(texture, NULL, data, x*4);
	SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
	stbi_image_free(data);
	uint32_t id = PutTextureInVector(texture);
	return new Texture(this, id, x, y);
}

void Renderer_SDL2::DestroyTexture(Texture * texture) {
	uint32_t id = texture->GetId();
	if(textures[id] == NULL) {
		return;
	}
	SDL_DestroyTexture(textures[id]);
	textures[id] = NULL;
}

void Renderer_SDL2::SetSize(unsigned int newWidth, unsigned int newHeight) {
	Renderer::SetSize(newWidth, newHeight);
	if(canvas != NULL) {
		SDL_DestroyTexture(canvas);
	}
	canvas = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_TARGET, width, height);
}

void Renderer_SDL2::SetTitle(std::string newTitle) {
	title = newTitle;
	SDL_SetWindowTitle(window, title.c_str());
}

unsigned int Renderer_SDL2::GetWindowWidth() {
	int w;
	SDL_GetWindowSize(window, &w, NULL);
	return w;
}

unsigned int Renderer_SDL2::GetWindowHeight() {
	int h;
	SDL_GetWindowSize(window, NULL, &h);
	return h;
}
