#include "boiler.h"
#include "SDL2/SDL.h"

using namespace Boiler;

Input_SDL2::Input_SDL2(Engine * engine) : Input(engine) {
	SDL_Init(SDL_INIT_EVENTS | SDL_INIT_JOYSTICK);
}

void Input_SDL2::Update() {
	requestedFullscreen = false;
	keys.clear();
	SDL_Event event;
	while(SDL_PollEvent(&event)) {
		if(event.type == SDL_MOUSEMOTION) {
			mouseX = ((float)event.motion.x / (float)engine->GetRenderer()->GetWindowWidth()) * engine->GetRenderer()->GetWidth();
			mouseY = ((float)event.motion.y / (float)engine->GetRenderer()->GetWindowHeight()) * engine->GetRenderer()->GetHeight();
		}
		if(event.type == SDL_MOUSEBUTTONDOWN) {
			if(event.button.button == SDL_BUTTON_LEFT) {
				mouseLeft = true;
			} else if(event.button.button == SDL_BUTTON_RIGHT) {
				mouseRight = true;
			}
		} else if(event.type == SDL_MOUSEBUTTONUP) {
			if(event.button.button == SDL_BUTTON_LEFT) {
				mouseLeft = false;
			} else if(event.button.button == SDL_BUTTON_RIGHT) {
				mouseRight = false;
			}
		} else if(event.type == SDL_KEYDOWN) {
			if((event.key.keysym.mod & KMOD_ALT) && event.key.keysym.sym == SDLK_RETURN) {
				requestedFullscreen = true;
			} else {
				keys[keycodeNameLookup[event.key.keysym.sym]] = true;
				keysDown[keycodeNameLookup[event.key.keysym.sym]] = true;
			}
		} else if(event.type == SDL_KEYUP) {
			keysDown[keycodeNameLookup[event.key.keysym.sym]] = false;
		} else if(event.type == SDL_QUIT) {
			requestedQuit = true;
		}
	}
}

bool TryGetKeycode(SDL_Keycode * key, std::string name) {
	SDL_Keycode sdlkey = SDL_GetKeyFromName(name.c_str());
	if(sdlkey == SDLK_UNKNOWN) {
		return false;
	} else {
		*key = sdlkey;
		return true;
	}
}

bool Input_SDL2::SetKey(std::string key, std::string name) {
	SDL_Keycode keycode;
	if(TryGetKeycode(&keycode, key)) {
		keycodeNameLookup[keycode] = name;
		return true;
	} else {
		engine->Log("Failed to bind key " + key + " to name " + name, 1, true);
		return false;
	}
}
