#include "boiler.h"

using namespace Boiler;

Tilemap::Tilemap(uint32_t newWidth, uint32_t newHeight, double newTilesize, byte defaultValue) {
	width = newWidth;
	height = newHeight;
	tilesize = newTilesize;
	tiledata = new byte[width * height];
	collisiondata = new byte[width * height];
	for(uint32_t y = 0; y < height; y++) {
		for(uint32_t x = 0; x < width; x++) {
			SetTile(x, y, defaultValue);
			SetTileCollision(x, y, 0);
		}	
	}
	outsideTile = 1;
	outsideCollision = true;
}

Tilemap::~Tilemap() {
	delete[] tiledata;
}

void Tilemap::SetTile(uint32_t x, uint32_t y, byte value) {
	if(x > width || y > height) return;
	tiledata[y * width + x] = value;
}

void Tilemap::SetTileAtWorld(double x, double y, byte value) {
	uint32_t ix = (uint32_t)(x / tilesize);
	uint32_t iy = (uint32_t)(y / tilesize);
	SetTile(ix, iy, value);
}

byte Tilemap::GetTile(uint32_t x, uint32_t y) {
	if(x > width || y > height) return outsideTile;
	return tiledata[y * width + x];
}

byte Tilemap::GetTileAtWorld(double x, double y) {
	uint32_t ix = (uint32_t)(x / tilesize);
	uint32_t iy = (uint32_t)(y / tilesize);
	return GetTile(ix, iy);
}

void Tilemap::SetTileCollision(uint32_t x, uint32_t y, byte collision) {
	if(x > width || y > height) return;
	collisiondata[y * width + x] = collision;
}

void Tilemap::SetTileCollisionAtWorld(double x, double y, byte collision) {
	uint32_t ix = (uint32_t)(x / tilesize);
	uint32_t iy = (uint32_t)(y / tilesize);
	SetTileCollision(ix, iy, collision);
}

byte Tilemap::GetTileCollision(uint32_t x, uint32_t y) {
	if(x > width || y > height) return outsideCollision;
	return collisiondata[y * width + x];
}

byte Tilemap::GetTileCollisionAtWorld(double x, double y) {
	uint32_t ix = (uint32_t)(x / tilesize);
	uint32_t iy = (uint32_t)(y / tilesize);
	return GetTileCollision(ix, iy);
}

uint8_t Tilemap::MoveAABBAgainstTilemap(double x, double y, double width, double height, double deltaX, double deltaY, double * outX, double * outY) {
	uint8_t collision = TILEMAP_COLLIDED_NONE;

	double newX = x + deltaX;
	double newY = y + deltaY;
	double xDir = deltaX > 0 ? 1 : -1;
	double yDir = deltaY > 0 ? 1 : -1;
	uint32_t ix = (uint32_t)floor((x + deltaX) / tilesize);
	uint32_t iy = (uint32_t)floor((y + deltaY) / tilesize);

	/*
	* This tests for tile-collisions at the edges of the gameObject
	* It tests at the top and at the bottom edge (with a little safety distance)
	*     ---------
	*     | *   * |*--->
	*     |   *   |
	*     | ***** |*--->
	*     ---------
	*/
	byte xCollision = GetTileCollisionAtWorld(newX + width*xDir/2.0, y - height/2.0 + 1);
	if(xCollision == TILEMAP_COLLISION_NONE) xCollision = GetTileCollisionAtWorld(newX + width*xDir/2.0, y + height/2.0 - 1);
	if(xCollision == TILEMAP_COLLISION_SOLID) {
		/*
		* Closes the remaining gap between GameObject and wall
		* This is done by calculating the position of the walls-edge and adding/subtracting
		* the GameObject's radius
		*/
		if(deltaX > 0) {
			newX = (ix+1)*tilesize - width/2.0;
			collision |= TILEMAP_COLLIDED_LEFT;
		} else {
			newX = (ix)*tilesize + width/2.0;
			collision |= TILEMAP_COLLIDED_RIGHT;
		}
	}

	/*
	* Same as above, but in y direction
	*     ---------
	*     | *   * |
	*     |   *   |
	*     | ***** |
	*     *-------*
	*      |     |
	*      |     |
	*     \/     \/
	*/
	byte yCollision = GetTileCollisionAtWorld(x - width/2.0 + 1, newY + height*yDir/2.0);
	if(yCollision == TILEMAP_COLLISION_NONE) yCollision = GetTileCollisionAtWorld(x + width/2.0 - 1, newY + height*yDir/2.0);
	if(yCollision != TILEMAP_COLLISION_NONE) {
		if(deltaY > 0) {
			newY = (iy+1)*tilesize - height/2.0;
			collision |= TILEMAP_COLLIDED_DOWN;
		} else if(yCollision != TILEMAP_COLLISION_JUMPTHROUGH) {
			newY = (iy)*tilesize + height/2.0;
			collision |= TILEMAP_COLLIDED_UP;
		}
	}

	*outX = newX;
	*outY = newY;
	return collision;
}