#include "boiler.h"

using namespace Boiler;

AudioClip::AudioClip(Audio * newAudio, uint32_t newId) {
    audio = newAudio;
    id = newId;
}

AudioClip::~AudioClip() {
    audio->UnloadClip(this);
}
