#include "boiler.h"

using namespace Boiler;

GuiElement::GuiElement(Renderer * newRenderer, std::string newFont) {
	renderer = newRenderer;
	font = newFont;
	texture = NULL;
	text = "";
	visible = true;
	r = 255;
	g = 255;
	b = 255;
	a = 255;
}

GuiElement::~GuiElement() {
	if(texture != NULL) {
		delete texture;
	}
}

void GuiElement::Update() {
	if(texture != NULL) {
		delete texture;
	}
	//texture = renderer->RenderTextToTexture(text, font);
}

void GuiElement::SetColor(unsigned char nr, unsigned char ng, unsigned char nb, unsigned char na) {
	r = nr;
	g = ng;
	b = nb;
	a = na;
	Update();
}

void GuiElement::SetContent(std::string newText) {
	text = newText;
	Update();
}

Gui::Gui(Engine * newEngine) {
	engine = newEngine;
}

Gui::~Gui() {

}

void Gui::AddElement(GuiElement * element) {
	elements.push_back(element);
}

void Gui::RemoveElement(GuiElement * element) {
	elements.remove(element);
	/* Automatic Deletion is taken out for now, as you'd need a reference
	to the GuiElement anyways. Also it would cause wierdness in other places. */
	//delete element;
}

void Gui::Draw() {
	for(auto it = elements.begin(); it != elements.end(); ++it) {
		GuiElement * element = *it;
		if(element->visible && element->GetTexture() != NULL) {
			engine->GetRenderer()->SetDrawColor(element->r, element->g, element->b);
			engine->GetRenderer()->DrawTexture(element->GetTexture(), element->x, element->y);
			engine->GetRenderer()->SetDrawColor(255, 255, 255);
		}
	}
}
