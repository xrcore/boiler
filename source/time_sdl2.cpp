#include "boiler.h"
#include "SDL2/SDL.h"

using namespace Boiler;

Time_SDL2::Time_SDL2(Engine * engine) : Time(engine) {
	SDL_Init(SDL_INIT_TIMER);
	lastFrameTicks = 0;
	ticks = 0;
}

Time_SDL2::~Time_SDL2() {

}

void Time_SDL2::Update() {
	lastFrameTicks = ticks;
	ticks = SDL_GetTicks();
	time = (double)ticks / 1000.0;
	deltaTime = time - ((double)lastFrameTicks / 1000.0);
}

void Time_SDL2::WaitForEndOfFrame() {
	/*uint32_t ticksPassed = SDL_GetTicks() - ticks;
	uint32_t frameTicks = (1000 / framerate);
	uint32_t ticksLeft = frameTicks - ticksPassed;
	if(ticksLeft <= frameTicks) {
		SDL_Delay(ticksLeft);
	}*/
	//SDL_Delay(1);
}
